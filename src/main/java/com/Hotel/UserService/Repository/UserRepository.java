package com.Hotel.UserService.Repository;

import com.Hotel.UserService.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long>{
    User findUsersByEmail(String email);
}
