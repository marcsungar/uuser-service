package com.Hotel.UserService.Services;

import com.Hotel.UserService.Entities.User;
import com.Hotel.UserService.Repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User getUser(String user) {
        return userRepository.findUsersByEmail(user);
    }

    public void deleteUser(String email) {
        User delete = userRepository.findUsersByEmail(email);
        userRepository.delete(delete);
    }

}
